<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends Controller
{

    /**
     * @Route("/contact", name="contact", methods={"POST"})
     */
    public function traitementAction(Request $request)
    {
        $request = Request::createFromGlobals();
        $objet = 'PAG';
        if('dev' === $this->get('kernel')->getEnvironment()) {
            $objet .= ' développement';
        }
        $message = $request->request->get('identite')." : ".$request->request->get('message');

        $mail = \Swift_Message::newInstance()
            ->setSubject($objet)
            ->setFrom('test@example.org')
            ->setBody($message);
        $retour = $this->get('mailer')->send($mail);
        return $this->render('contact/contact.html.twig', array('confirmation' => $retour));
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        return $this->render('contact/contact.html.twig');
    }

}
