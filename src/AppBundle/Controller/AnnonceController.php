<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Form\AnnonceType;
use AppBundle\Entity\Annonce;

class AnnonceController extends Controller
{

    /**
     * @Route("/annonces", name="annonces")
     */
    public function annoncesAction(Request $request)
    {
        $annonces = $this->getDoctrine()
            ->getRepository('AppBundle:Annonce')
            ->findAll();

        return $this->render('annonces/annonces.html.twig', array('annonces' => $annonces));
    }

    /**
     * @Route("/annonce/{id}", name="annonce")
     */
    public function annonceAction(Request $request, $id)
    {   
        $annonce = $this->getDoctrine()
            ->getRepository('AppBundle:Annonce')
            ->find($id);
            
        return $this->render('annonces/annonce.html.twig', array('annonce' => $annonce));
    }

    /**
     * @Route("/ajouter-annonce", name="ajouter-annonce")
     */
    public function annonceAjoutAction(Request $request)
    {   
        $annonce = new Annonce();
        $form=$this->createForm(AnnonceType::class, $annonce);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid() ){
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($annonce);
            $em->flush();
            return $this->redirect($this->generateUrl('annonces'));
        }
        return $this->render('annonces/ajout.html.twig', array('form' => $form->createView()));
    }

}
