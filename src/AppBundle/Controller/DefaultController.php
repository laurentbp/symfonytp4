<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="home")
     */
    public function homeAction(Request $request)
    {
        return $this->render('home.html.twig');
    }

    /**
     * @Route("/conditions", name="conditions")
     */
    public function conditionsAction(Request $request)
    {
        return $this->render('conditions/conditions.html.twig');
    }
}
