<?php

// src/AppBundle/Twig/AnnonceExtension.php

namespace AppBundle\Twig;

class AnnonceExtension extends \Twig_Extension {
	public function getFilters(){
		return array(new \Twig_SimpleFilter('prix',array($this, 'prixFilter'),array('is_safe' => array('html'))));
	}

	public function prixFilter($prix){
		if($prix<50){
			return '<span class="low-price">'.$prix.' €</span>';
		}
		elseif($prix>=50 && $prix<500){
			return '<span class="middle-price">'.$prix.' €</span>';
		}
		elseif($prix>=500){
			return '<span class="high-price">'.$prix.' €</span>';
		}
	}

	public function getName(){
		return 'article_extension';
	}
}